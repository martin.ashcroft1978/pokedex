<?php
use PHPUnit\Framework\TestCase;
use App\Api\PokemonApi;

final class PokemonApiTest extends TestCase
{
    private $pokemonApi;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        $this->pokemonApi = new PokemonApi();
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @throws Exception
     */
    public function testIdIsNotNumeric()
    {
        $this->expectException(Exception::class);

        $this->pokemonApi->getPokemon('INVALID_ID');
    }

    /**
     * @throws Exception
     */
    public function testIdIsZero()
    {
        $this->expectException(Exception::class);

        $this->pokemonApi->getPokemon(0);
    }
}
