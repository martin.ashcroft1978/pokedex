<?php
namespace App\Api;

use GuzzleHttp\Client;
use Exception;
use Predis\Client as PredisClient;
use PokePHP\PokeApi;

/**
 * Created by PhpStorm.
 * User: martinashcroft
 * Date: 29/07/2019
 * Time: 18:25
 */

/**
 * Class PokemonApi
 * @package App\Api
 */
class PokemonApi
{
    /**
     * @return \Psr\Http\Message\StreamInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAllPokemon()
    {
        $redisKey = 'all';
        $predisClient = new PredisClient();
        $redisData = $predisClient->get($redisKey);

        if (!(empty($redisData))) {
            $body = $redisData;
        } else {
            /**
             * Can't seem to find a method that gets all the records so I am just
             * using my own with Guzzle
             */
            $client = new Client([
                'base_uri' => 'https://pokeapi.co/api/v2/',
                'timeout' => 2.0,
            ]);

            $response = $client->request('GET', 'pokemon?limit=1000');
            $body = $response->getBody();
            $predisClient->setex($redisKey, 86400, $body);
        }

        return $body;
    }

    /**
     * @param $id
     * @return false|string
     * @throws Exception
     */
    public function getPokemon($id)
    {
        if (!is_numeric($id)) {
            throw new Exception('ID must be numeric');
        }

        if (!$id > 0){
            throw new Exception('ID must be greater than zero');
        }

        $predisClient = new PredisClient();
        $redisData = $predisClient->get($id);

        if (!empty($redisData)) {
            $data = $redisData;
        } else {
            $directoryLocation =
                'images' .
                DIRECTORY_SEPARATOR .
                $id;

            if (!file_exists($directoryLocation)) {
                mkdir($directoryLocation);
            }

            $api = new PokeApi;
            $pokemon = $api->pokemon($id);
            $pokemon = json_decode($pokemon, true);
            $sprites = $pokemon['sprites'];

            if (is_array($sprites)) {
                foreach ($sprites as $key => $imageLocation) {
                    if (!empty($imageLocation)) {
                        $imageLocationBits = explode('.', $imageLocation);
                        $extension = end($imageLocationBits);
                        $localFileName = $directoryLocation . DIRECTORY_SEPARATOR . $key . '.' . $extension;
                        if (!file_exists($localFileName)) {
                            copy($imageLocation, $localFileName);
                        }
                        $sprites[$key] = DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $key . '.' . $extension;
                    } else {
                        unset($sprites[$key]);
                    }
                }
            }

            $pokemon['sprites'] = $sprites;
            $data = json_encode($pokemon);
            $predisClient->setex($id, 86400, $data);
        }
        return $data;
    }
}