<?php
namespace App;

use App\Output\OutputInterface;
use App\Output\DataTableOutput;
use App\Api\PokemonApi;
use App\Output\DataTableRowDataOutput;
use Exception;

/**
 * Created by PhpStorm.
 * User: martinashcroft
 * Date: 29/07/2019
 * Time: 18:23
 */

/**
 * Class Client
 * @package App
 */
Class Client
{
    private $output;

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPokemonTableData()
    {
        try {
            $pokemonApi = new PokemonApi();
            $this->setOutput(new DataTableOutput());
            $data = $pokemonApi->getAllPokemon();
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return $this->output->load($data);
    }

    /**
     * @param $id
     * @return string
     */
    public function getPokemonTableRowData($id)
    {
        try {
            $pokemonApi = new PokemonApi();
            $this->setOutput(new DataTableRowDataOutput());
            $data = $pokemonApi->getPokemon($id);
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return $this->output->load($data);
    }

    /**
     * @param OutputInterface $outputType
     */
    public function setOutput(OutputInterface $outputType)
    {
        $this->output = $outputType;
    }

}