<?php
namespace App\Output;
/**
 * Created by PhpStorm.
 * User: martinashcroft
 * Date: 29/07/2019
 * Time: 18:09
 */

/**
 * Interface OutputInterface
 * @package App\Interfaces
 */
interface OutputInterface {
    public function load($data);
}