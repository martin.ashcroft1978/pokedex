<?php
namespace App\Output;
/**
 * Created by PhpStorm.
 * User: martinashcroft
 * Date: 29/07/2019
 * Time: 18:09
 */

/**
 * Class DataTableOutput
 * @package App\Interfaces
 */
class DataTableOutput implements OutputInterface
{
    /**
     * @param $data
     * @return string
     */
    public function load($data)
    {
        $pokies = json_decode($data, true);

        foreach ($pokies['results'] as $key => $details) {
            $url = $details['url'];
            $name = $details['name'];
            $re = '/https:\/\/pokeapi.co\/api\/v2\/pokemon\/(\d*)\//m';
            $str = $url;
            preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);
            $id = $matches[0][1];
            $contacts[] = [
                'name' => $name,
                'url' => $url,
                'id' => $id
            ];
        }
        $data = [
            'data' => $contacts
        ];

        return json_encode($data);
    }
}