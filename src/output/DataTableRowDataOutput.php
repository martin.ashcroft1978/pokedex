<?php
namespace App\Output;
/**
 * Created by PhpStorm.
 * User: martinashcroft
 * Date: 29/07/2019
 * Time: 18:09
 */

/**
 * Class DataTableRowDataOutput
 * @package App\Interfaces
 */
class DataTableRowDataOutput implements OutputInterface
{
    /**
     * @param $data
     * @return string
     */
    public function load($data)
    {
        $data = json_decode($data, true);
        $html = '<table cellpadding="5" cellspacing="0" border="0" style="margin-left:150px;">';
        $html.= '<tr><td>Name</td><td>' . $data['name'] . '</td></tr>';
        $html.= '<tr><td>Height</td><td>' . $data['height'] . '</td></tr>';
        $html.= '<tr><td>Weight</td><td>' . $data['weight'] . '</td></tr>';
        $html.= '<tr><td>Species</td><td>' . $data['species']['name'] . '</td></tr>';
        $html.= '<tr><td>Abilities</td><td><ul class="list-group list-group-flush">';
        foreach ($data['abilities'] as $ability) {
            $html.= '<li class="list-group-item">' . $ability['ability']['name'] . '</li>';
        }
        $html.= '</ul></td></tr>';
        $html.= '<tr><td colspan="2">';
        foreach ($data['sprites'] as $imgHref) {
            $html.='<img src="' . $imgHref . '" />';
        }
        $html.= '</td></tr>';
        $html.= '</table>';
        return $html;
    }
}