<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pokédex</title>
        <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.foundation.min.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" charset="utf8" src="js/main.js"></script>
    </head>
    <body>
    <div class="container">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">Pokédex</h1>
            </div>

        </div>
        <table id="pokemon" class="display" style="width:100%">
            <thead>
            <tr>
                <th></th>
                <th>Name</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th></th>
                <th>Name</th>
            </tr>
            </tfoot>
        </table>
    </div>
    </body>
</html>