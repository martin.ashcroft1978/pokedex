/* Formatting function for row details - modify as you need */
function format ( d ) {
    var div = $('<div/>')
        .text( 'Loading...' );

    $.ajax( {
        url: 'pokemon.php',
        method: "POST",
        data: { url: d.url, id: d.id },
        success: function ( data ) {
            console.log(data);
            div.html( data );
        }
    } );

    return div;
}

$(document).ready(function() {
    var table = $('#pokemon').DataTable( {
        "ajax": "all.php",
        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { "data": "name" },
        ],
        "order": [[1, 'asc']]
    } );

    // Add event listener for opening and closing details
    $('#pokemon tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
} );